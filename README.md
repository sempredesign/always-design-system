# ALWAYS #

O Design System base da Sempre Design.

### Para que serve este repositório? ###

Este repositório serve de base para os Design Systems desenvolvidos para os projetos da Sempre Design e seus clientes.

O Always possui todos os elementos essenciais para um Design System padrão, 
porém com uma camada separada de aparência. O foco é na estrutura e nos elementos. 

Desacoplado dos estilos, o Always é reutilizável. Basta substituir as configurações 
de estilos, tais como cores, tipos de letra, ícones, metáfora, sombras, volume, 
entre outros, para dar vida e personalidade a qualquer organização, produto ou serviço.

## Princípios do Always ##

### Continuidade

Novos elementos, componentes e estruturas de páginas são incluídos ou melhorados à medida que são demandados pelos projetos.


### Abrangência

Que seja suscinto porém, ambicione ter o máximo de elementos necessários para servir de base para outros Design Systems. 


### Consistência

Evitar erros e primar pela simplicidade e bom funcionamento dos componentes, a ponto de transmitir segurança, tanto por designers, usuários e desenvolvedores.


### Flexibilidade

Capaz de se adaptar com facilidade às mudanças de estilo e tecnologia e permitir o reuso de forma simples.


### Nitidez

Conceitos, compontentes visuais, códigos-fonte e demais elementos bem definidos e delimitados, sem ambiguidades e informações desnecessárias.

### Usabilidade

O **Always** deve primar pela facilidade de uso, não só do próprio Design System como produto para desenvolvedores e designers mas na concepção de cada componente para o usuário final que interage com os produtos e serviços que o utilizam como base.


## Este Design System inclui:

* Definições e conceitos
* Componentes de interface escritos em HTML, CSS e JavaScript.
* Variávels de estilo (Design Tokens) em CSS puro.
* Nomenclatura semântica dos elementos e componentes. 
* Humer, tom de voz e características da persona.
* Uma estrutura de navegação para visualização dos elementos, componentes e páginas.
* Montador de páginas, onde é possível escolher o modelo de página e incluir componentes nas áreas.

